﻿namespace DataAccess.Model
{
    public class Food
    {
        private int Id { get; set; }
        private int Cena { get; set; }
        private string Nazev { get; set; }
        private int Hmotnost { get; set; }
        private Kategorie Kategorie { get; set; }
    }

    public class Kategorie
    {
        private string Nazev { get; set; }
        private Kategorie Subkategorie { get; set; }
    }
}