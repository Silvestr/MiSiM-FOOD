﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MiSiM.Startup))]

namespace MiSiM
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}