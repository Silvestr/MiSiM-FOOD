﻿using System.Collections.Generic;

namespace MiSiM.Models
{
    public class Food
    {
        public int Id { get; set; }
        public float Cena { get; set; }
        public string Nazev { get; set; }
        public float Hmotnost { get; set; }
        public Kategorie Kategorie { get; set; }
        public Image Obrazek { get; set; }

        //contemporay
        public static List<Food> FakeList
        {
            get
            {
                List<Food> foodMenu = new List<Food>();
                foodMenu.Add(new Food() { Id = 1, Nazev = "Vega", Cena = 15, Hmotnost = 2 });
                foodMenu.Add(new Food() { Id = 2, Nazev = "Hrach", Cena = 15, Hmotnost = 5 });
                foodMenu.Add(new Food() { Id = 3, Nazev = "Gefo", Cena = 65, Hmotnost = 4 });
                foodMenu.Add(new Food() { Id = 4, Nazev = "Tufo", Cena = 15, Hmotnost = 5 });
                foodMenu.Add(new Food() { Id = 5, Nazev = "Kako", Cena = 55, Hmotnost = 1 });
                return foodMenu;
            }
        }
    }

    public class Image
    {
        private int Id { get; set; }
    }

    public class Kategorie
    {
        private int Id { get; set; }
        private string Nazev { get; set; }
        private Kategorie Subkategorie { get; set; }
    }
}