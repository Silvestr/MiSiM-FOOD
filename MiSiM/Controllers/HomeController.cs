﻿using System.Web.Mvc;

namespace MiSiM.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}